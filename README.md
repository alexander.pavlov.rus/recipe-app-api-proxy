# recipe-app-api-proxy

Recipe app API proxy application

## Usage

### Env Vars

* `LISTEN_PORT` - port to listen on (default `8000`)
* `APP_HOST` - hostname to forward to (default `app`)
* `APP_PORT` - port to forward to (default `9000`)
